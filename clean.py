#!/usr/bin/env python3
# encoding: utf-8

from glob import glob
from util import delete_file
import os
import os.path
import subprocess

def clean_backups(name):
    "Delete backup files from problem `name`'s tree."
    for dirpath, _, backup_fns in os.walk('%s/' % name):
        for backup_fn in backup_fns:
            if backup_fn.endswith('~'):
                delete_file(os.path.join(dirpath, backup_fn))

def clean_build(name):
    subprocess.check_call(['rm', '-rf', '%s/build/tests/' % name])

def box_clean(name):
    "Clean up problem tree, deleting all generated files."
    for pdf_fn in glob(f'{name}/documents/*.pdf'):
        # preserve files named submission.pdf
        _, tail_fn = os.path.split(pdf_fn)
        if tail_fn != 'submission.pdf':
            delete_file(pdf_fn)
    for log_fn in glob(f'{name}/documents/*.log'):
        delete_file(log_fn)
    delete_file(f'{name}/{name}.in')
    delete_file(f'{name}/{name}.sol')
    for folder in ['solutions/*', 'attic', 'checkers', 'judge']:
        for fn_regex in ['*.exe', '*.jexe', '*.class', '*.pyc', '.cache/*']:
            for fn in glob(f'{name}/{folder}/{fn_regex}'):
                delete_file(fn)
    clean_backups(name)
    clean_build(name)

